from django.shortcuts import render

def index(request):
    return render(request, 'homestayapp/index.html')
