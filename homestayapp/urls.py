"""homestayapp URL Configuration"""
from django.conf.urls import url, include
from django.contrib import admin

from . import views as main_views
from booking.views import new_customer_reservation as ncr
from booking.views import new_customer_reservation_2 as ncr2
from booking.views import list_reservation_json

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', main_views.index, name='index'),
    url(r'^app/', include('booking.urls', namespace='booking')),
    url(r'^user/', include('user.urls', namespace='user')),
    # Special link for customers only
    url(r'^(?P<property_slug>[\w-]+)/$', ncr, name='new_cust_reservation'),
    url(r'^(?P<property_slug>[\w-]+)/page2/(?P<reservation_id>\d+)/$', ncr2, name='new_cust_reservation_2'),
    # API link for listing
    url(r'^api/(?P<property_slug>[\w-]+)/$', list_reservation_json, name='list_reservation_json'),
]
