from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Owner(models.Model):
    # Hidden from form
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    date_added = models.DateTimeField(auto_now_add=True)
    # Superadmin only
    is_premium = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    # Required
    full_name = models.CharField(max_length=150)
    phone_number = models.CharField(max_length=20, verbose_name='phone number')
    # Optional
    company_name = models.CharField(max_length=150,blank=True)
    company_address = models.CharField(max_length=400,blank=True)
    # TODO: bank acc dan lain2

    def __str__(self):
        """Return a string representation of the model."""
        return u'%s %s (%s)' % (self.full_name,
                                self.company_name,self.id)
