from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout, login, authenticate, update_session_auth_hash
from django.contrib.auth.forms import AdminPasswordChangeForm, PasswordChangeForm
from django.contrib import messages
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from .models import Owner
from .forms import OwnerForm, UserCreationForm, UserChangeForm


def logout_view(request):
    """Log the user out"""
    logout(request)
    return HttpResponseRedirect(reverse('index'))

def register(request):
    """Register a new user"""
    if request.method != 'POST':
        # Display blank registration form
        form = UserCreationForm()
        owner_form = OwnerForm()
    else:
        # Process completed form
        form = UserCreationForm(data=request.POST)
        owner_form = OwnerForm(data=request.POST)

        if form.is_valid() and owner_form.is_valid():
            new_user = form.save()
            new_owner = owner_form.save(commit=False)
            new_owner.user = new_user
            new_owner.save()
            # Log the user in and then redirect to home page.
            authenticate_user = authenticate(username=new_user.username,
                password=request.POST['password1'])
            login(request, authenticate_user)
            return HttpResponseRedirect(reverse('booking:index'))
    context = {'form': form, 'owner_form': owner_form}
    return render(request, 'user/register.html', context)

@login_required
def settings(request):
    """Edit settings for a user"""
    user = request.user
    owner = Owner.objects.get(user=user)

    if request.method != 'POST':
        # Display blank registration form
        form = UserChangeForm(instance=user)
        owner_form = OwnerForm(instance=owner)
    else:
        # Process completed form
        form = UserChangeForm(data=request.POST,instance=user)
        owner_form = OwnerForm(data=request.POST,instance=owner)

        if form.is_valid() and owner_form.is_valid():
            new_user = form.save()
            new_owner = owner_form.save(commit=False)
            new_owner.user = new_user
            new_owner.save()
            return HttpResponseRedirect(reverse('booking:index'))
    context = {'form': form, 'owner_form': owner_form}
    return render(request, 'user/settings.html', context)
