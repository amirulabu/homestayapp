from django.contrib import admin
from .models import Owner

class OwnerAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'company_name')

admin.site.register(Owner, OwnerAdmin)
