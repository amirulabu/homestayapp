from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

from .models import Owner

class OwnerForm(forms.ModelForm):
    class Meta:
        model = Owner
        fields = [
            # Required for customer
            'full_name',
            'phone_number',
            # Optional for customer
            'company_name',
            'company_address'
        ]
        labels = {
            'company_name': 'Company name(optional)',
            'company_address': 'Company address(optional)',
        }

class UserCreationForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = [
            "email",
            "username",
            "password1",
            "password2"
         ]

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user

class UserChangeForm(forms.ModelForm):
    class Meta:
        model = User
        fields = [
            "email",
            "username",
         ]

    def save(self, commit=True):
        user = super(UserChangeForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user
