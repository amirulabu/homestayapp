"""Defines URL patterns for user"""
from django.conf.urls import url
from django.contrib.auth.views import login, password_change

from . import views

urlpatterns = [
    # Logout page
    url(r'^logout/$', views.logout_view, name='logout'),
    # Login page
    url(r'^login/$', login, {'template_name': 'user/login.html'},name='login'),
    # Show settings
    url(r'^settings/$', views.settings, name='settings'),
    # Setting up password
    url(r'^password/$', password_change,{'template_name': 'user/password.html','post_change_redirect': 'index'}, name='password'),
    # Register user
    url(r'^register/$', views.register, name='register'),
]
