"""Defines URL patterns for homestay"""

from django.conf.urls import url
from . import views

urlpatterns = [
    # Dashboard/manage, shows upcoming reservation and all property
    url(r'^$', views.index, name='index'),
    # CRUD property # no delete, just "archive"
    url(r'^property/new/$', views.new_property, name='new_property'),
    url(r'^property/(?P<property_id>\d+)/$', views.property, name='property'),
    url(r'^property/(?P<property_id>\d+)/edit/$', views.edit_property, name='edit_property'),
    # CRUD reservation # no delete, just "archive"
    url(r'^property/(?P<property_id>\d+)/reservation/new/$', views.new_reservation, name='new_reservation'),
    url(r'^reservation/(?P<reservation_id>\d+)/$', views.reservation, name='reservation'),
    url(r'^reservation/(?P<reservation_id>\d+)/edit/$', views.edit_reservation, name='edit_reservation'),
    url(r'^reservation/(?P<reservation_id>\d+)/editstatus/$', views.edit_status_reservation, name='edit_status_reservation'),
    url(r'^reservation/(?P<reservation_id>\d+)/delete/$', views.delete_reservation, name='delete_reservation'),
    url(r'^reservation/(?P<reservation_id>\d+)/restore/$', views.restore_reservation, name='restore_reservation'),
    # JSON reservation
    url(r'^reservation/api/(?P<property_slug>[\w-]+)/$', views.reservation_json, name='reservation_json'),
    # List reservation
    url(r'^reservation/past/$', views.list_past_reservation, name='list_past_reservation'),
    url(r'^reservation/upcoming/$', views.list_upcoming_reservation, name='list_upcoming_reservation'),
    url(r'^reservation/deleted/$', views.list_deleted_reservation, name='list_deleted_reservation'),
    # Download CSV reservation
    url(r'^reservation/download-csv/$', views.download_reservation_csv, name='download_reservation_csv'),
    # Calendar for overall view of reservations
    url(r'^calendar/$', views.calendar, name='calendar'),
]
