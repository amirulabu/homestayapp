from django.shortcuts import render,get_object_or_404
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.core.urlresolvers import reverse
from django.core import serializers
from django.contrib.auth.decorators import login_required
import itertools
from django.utils.text import slugify

from .models import Property, Reservation
from .forms import PropertyForm, ReservationForm, CustomerReservationForm, ReservationStatusForm

import datetime, json, csv

@login_required
def index(request):
    """Dashboard page"""

    reservations = Reservation.objects.filter(property_obj__owner=request.user)
    now_hosting_reservations = reservations.filter(check_in_date__lte=datetime.date.today()
                                          ).filter(check_out_date__gt=datetime.date.today()
                                          ).order_by('-check_out_date')
    upcoming_reservations = reservations.filter(check_in_date__gt=datetime.date.today()
                                       ).order_by('check_in_date')[:5]
    properties = Property.objects.filter(owner=request.user)

    context = {
        'now_hosting_reservations': now_hosting_reservations,
        'properties': properties,
        'upcoming_reservations': upcoming_reservations,
    }
    return render(request, 'booking/index.html', context)



@login_required
def property(request, property_id):
    """Show details of a property"""
    property_obj = get_object_or_404(Property, id=property_id)
    if property_obj.owner != request.user:
        raise Http404
    context = {'property_obj': property_obj}
    return render(request, 'booking/property.html', context)

@login_required
def new_property(request):
    """Add a new property"""
    if request.method != 'POST':
        # No data is submitted; create a blank form
        form = PropertyForm()
    else:
        # POST data is submitted; process data
        form = PropertyForm(data=request.POST)
        if form.is_valid():
            new_property = form.save(commit=False)
            new_property.owner = request.user
            max_length = Property._meta.get_field('slug').max_length
            new_property.slug = orig = slugify(new_property.name)[:max_length]
            for x in itertools.count(1):
                if not Property.objects.filter(slug=new_property.slug).exists():
                    break
                # Truncate the original slug dynamically. Minus 1 for the hyphen.
                new_property.slug = "%s-%d" % (orig[:max_length - len(str(x)) - 1], x)
            new_property.save()
            return HttpResponseRedirect(reverse('booking:property', args=[new_property.id]))
    context = {'form': form}
    return render(request, 'booking/new_property.html', context)

@login_required
def edit_property(request, property_id):
    """Edit a property"""
    property_obj = Property.objects.get(id=property_id)
    if property_obj.owner != request.user:
        raise Http404
    if request.method != 'POST':
        # No data is submitted; create a blank form
        form = PropertyForm(instance=property_obj)
    else:
        # POST data is submitted; process data
        form = PropertyForm(instance=property_obj,data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('booking:property', args=[property_obj.id]))
    context = {'property_obj': property_obj, 'form': form}
    return render(request, 'booking/edit_property.html', context)


@login_required
def reservation(request, reservation_id):
    """Show details of a reservation"""
    reservation = get_object_or_404(Reservation, id=reservation_id)
    if reservation.property_obj.owner != request.user:
        raise Http404
    context = {'reservation': reservation}
    return render(request, 'booking/reservation.html', context)

@login_required
def new_reservation(request, property_id):
    """Admin - add a new reservation"""
    property_obj = get_object_or_404(Property, id=property_id)
    if request.method != 'POST':
        # No data is submitted; create a blank form
        form = ReservationForm()
    else:
        # POST data is submitted; process data
        form = ReservationForm(data=request.POST)
        if form.is_valid():
            new_reservation = form.save(commit=False)
            new_reservation.property_obj = property_obj
            new_reservation.save()
            new_bank_reference = str(datetime.date.today().__format__("%y%m")) + \
                              str(property_obj.id) + "-" +\
                              str(new_reservation.id)
            new_reservation.bank_reference = new_bank_reference
            new_reservation.save()
            return HttpResponseRedirect(reverse('booking:reservation', args=[new_reservation.id]))
    context = {'property_obj': property_obj, 'form': form}
    return render(request, 'booking/new_reservation.html', context)

@login_required
def edit_reservation(request, reservation_id):
    """Edit a reservation"""
    reservation = get_object_or_404(Reservation, id=reservation_id)
    # property_obj = reservation.property_obj
    if reservation.property_obj.owner != request.user:
        raise Http404
    if request.method != 'POST':
        # No data is submitted; create a blank form
        form = ReservationForm(instance=reservation)
    else:
        # POST data is submitted; process data
        form = ReservationForm(instance=reservation,data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('booking:reservation', args=[reservation.id]))
    context = {'reservation': reservation, 'form': form}
    return render(request, 'booking/edit_reservation.html', context)

@login_required
def edit_status_reservation(request, reservation_id):
    """Edit status reservation"""
    reservation = get_object_or_404(Reservation, id=reservation_id)
    if reservation.property_obj.owner != request.user:
        raise Http404
    if request.method != 'POST':
        # No data is submitted; create a blank form
        form = ReservationStatusForm(instance=reservation)
    else:
        # POST data is submitted; process data
        form = ReservationStatusForm(instance=reservation,data=request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('booking:reservation', args=[reservation.id]))
    context = {'reservation': reservation, 'form': form}
    return render(request, 'booking/edit_status_reservation.html', context)

@login_required
def delete_reservation(request, reservation_id):
    """ Add deleted_at datetime to Reservation, thus deleting it """
    reservation = get_object_or_404(Reservation, id=reservation_id)
    reservation.deleted_at = datetime.datetime.today()
    reservation.save()
    return HttpResponseRedirect(reverse('booking:index'))

@login_required
def list_deleted_reservation(request):
    """ List deleted Reservation to restore """
    reservations = Reservation.all_objects.all().dead().filter(property_obj__owner=request.user).order_by('-check_out_date')
    context = {'reservations': reservations}
    return render(request, 'booking/list_deleted_reservation.html', context)

@login_required
def restore_reservation(request, reservation_id):
    """ set deleted_at to none to restore reservation """
    if Reservation.all_objects.all().get(id=reservation_id):
        reservation = Reservation.all_objects.all().get(id=reservation_id)
        reservation.deleted_at = None
        reservation.save()
        return HttpResponseRedirect(reverse('booking:index'))
    else:
        raise Http404

@login_required
def list_past_reservation(request):
    """List past and currently serving reservation """
    reservations = Reservation.objects.filter(property_obj__owner=request.user)
    past_reservations = reservations.filter(check_in_date__lte=datetime.date.today()
                                          ).order_by('-check_out_date')
    context = {'past_reservations': past_reservations}
    return render(request, 'booking/list_past_reservation.html', context)

@login_required
def list_upcoming_reservation(request):
    """List past and currently serving reservation """
    reservations = Reservation.objects.filter(property_obj__owner=request.user)
    upcoming_reservations = reservations.filter(check_in_date__gt=datetime.date.today()
                                       ).order_by('check_in_date')
    context = {'upcoming_reservations': upcoming_reservations}
    return render(request, 'booking/list_upcoming_reservation.html', context)

def new_customer_reservation(request, property_slug):
    """Customer - add a new reservation"""
    property_obj = get_object_or_404(Property,slug=property_slug)
    reservations = Reservation.objects.filter(property_obj=property_obj,
                                              check_out_date__gt=datetime.date.today()
                                     ).order_by('check_in_date')
    # import pdb; pdb.set_trace()
    if request.method != 'POST':
        # No data is submitted; create a blank form
        form = CustomerReservationForm(property_obj=property_obj)
    else:
        # POST data is submitted; process data
        form = CustomerReservationForm(data=request.POST, property_obj=property_obj)
        if form.is_valid():
            new_reservation = form.save(commit=False)
            new_reservation.property_obj = property_obj
            new_reservation.save()
            new_bank_reference = str(datetime.date.today().__format__("%y%m")) + \
                              str(property_obj.id) + "-" +\
                              str(new_reservation.id)
            new_reservation.bank_reference = new_bank_reference
            new_reservation.save()
            return HttpResponseRedirect(reverse('new_cust_reservation_2', args=[property_obj.slug,new_reservation.id]))
    context = {'property_obj': property_obj, 'form': form, 'reservations':reservations}
    return render(request, 'booking/new_cust_reservation.html', context)

def new_customer_reservation_2(request, property_slug, reservation_id):
    """Customer - show banking information"""
    property_obj = get_object_or_404(Property,slug=property_slug)
    reservation = get_object_or_404(Reservation,id=reservation_id)
    # or aka Recipient Reference, maybe date with reservationsid?
    reservation_date_delete = reservation.date_added + datetime.timedelta(days=2)
    context = {'property_obj': property_obj, 'reservation':reservation, 'reservation_date_delete': reservation_date_delete}
    return render(request, 'booking/new_cust_reservation_2.html', context)


def list_reservation_json(request, property_slug):
    property_obj = get_object_or_404(Property,slug=property_slug)
    reservations = Reservation.objects.filter(property_obj=property_obj,
                                              check_out_date__gt=datetime.date.today()
                                     ).order_by('check_in_date')
    data = []
    for reservation in reservations:
        x = {}
        x['start'] = str(reservation.check_in_date)
        x['end'] = str(reservation.check_out_date - datetime.timedelta(days=1))
        data.append(x)
    return HttpResponse(json.dumps(data,sort_keys=True), content_type='application/json')

@login_required
def download_reservation_csv(request):
    reservations = Reservation.objects.filter(property_obj__owner=request.user
                                     ).order_by('check_in_date')
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'

    writer = csv.writer(response)
    writer.writerow([
        'property_obj',
        'bank_reference',
        'customer_name',
        'customer_email',
        'customer_phone_number',
        'check_in_date',
        'check_out_date',
        'payment',
        'customer_notes',
        'status',
        'notes'
    ])
    for reservation in reservations:
        writer.writerow([
            str(reservation.property_obj),
            str(reservation.bank_reference),
            str(reservation.customer_name),
            str(reservation.customer_email),
            str(reservation.customer_phone_number),
            str(reservation.check_in_date),
            str(reservation.check_out_date),
            str(reservation.payment),
            str(reservation.customer_notes),
            str(reservation.status),
            str(reservation.notes),
        ])

    return response

def calendar(request):
    """Dashboard page"""

    reservations = Reservation.objects.filter(property_obj__owner=request.user)
    now_hosting_reservations = reservations.filter(check_in_date__lte=datetime.date.today()
                                          ).filter(check_out_date__gt=datetime.date.today()
                                          ).order_by('-check_out_date')
    upcoming_reservations = reservations.filter(check_in_date__gt=datetime.date.today()
                                       ).order_by('check_in_date')[:5]
    properties = Property.objects.filter(owner=request.user)

    context = {
        'now_hosting_reservations': now_hosting_reservations,
        'properties': properties,
        'upcoming_reservations': upcoming_reservations,
    }
    return render(request, 'booking/calendar.html', context)

def reservation_json(request, property_slug):
    property_obj = get_object_or_404(Property,slug=property_slug)
    reservations = Reservation.objects.filter(property_obj=property_obj,
                                              check_out_date__gt=datetime.date.today()
                                     ).order_by('check_in_date')
    data = []
    for reservation in reservations:
        x = {}
        x['property'] = str(reservation.property_obj.name)
        x['slug'] = str(reservation.property_obj.slug)
        x['name'] = str(reservation.customer_name)
        x['id'] = str(reservation.id)
        x['ref'] = str(reservation.bank_reference)
        x['start'] = str(reservation.check_in_date)
        x['end'] = str(reservation.check_out_date)
        data.append(x)
    return HttpResponse(json.dumps(data,sort_keys=True), content_type='application/json')
