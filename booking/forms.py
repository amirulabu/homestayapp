from django import forms
import datetime


from .models import Reservation, Property

class PropertyForm(forms.ModelForm):
    class Meta:
        model = Property
        fields = [
            # Required
            'name',
            'location',
            'property_type',
            'max_occupant',
            'normal_price',
            'check_in_time',
            'check_out_time',
            'bank_name',
            'account_name',
            'account_number',
            # Optional
            'special_price',
            'website',
            'property_description',
            'terms_cond',
            # Premium
            'bulk_price',
            'offer_price',
            'offer_ongoing',
        ]
        help_texts = {
            'check_in_time':'HH:MM:SS in 24 hours format',
            'check_out_time':'HH:MM:SS in 24 hours format',
            'special_price':'(Optional) price when demand is slightly higher eg. weekends, holiday season',
            'website':'(Optional) Enter your website URL',
            'property_description':'(Optional) Describe your property, eg. how many bedrooms, bathrooms',
            'terms_cond':'(Optional)',
            'bank_name':'Bank name eg. CIMB Bank, Maybank',
            'account_name':'Bank account name',
            'account_number':'Bank account number',
            'max_occupant':'Maximum occupant at one time',
        }

class ReservationForm(forms.ModelForm):
    # def __init__(self, *args, **kwargs):
    #     self.property_obj = kwargs.pop('property_obj')
    #     super(ReservationForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Reservation
        fields = [
            # Required for customer
            'customer_name',
            'customer_email',
            'customer_phone_number',
            'check_in_date',
            'check_out_date',
            'payment',
            # For admin
            'status',
            # Optional for admin
            'notes',
        ]
        help_texts = {
            'notes':'(Optional) Additional notes, only property owner can see this',
            'check_in_date':'YYYY-MM-DD format',
            'check_out_date':'YYYY-MM-DD format',
        }

    # No validation for Admin because contradics with edit reservation. cannot save

    # def dates_is_avaliable(self,a, b):
    #     r1,r2,r3 = (Reservation.objects.filter(property_obj=self.property_obj),)*3
    #     checkout_overlap = r1.filter(check_out_date__gt=a, check_out_date__lte=b)
    #     checkin_overlap = r2.filter(check_in_date__gte=a, check_in_date__lt=b)
    #     both_overlap = r3.filter(check_in_date__lte=a, check_out_date__gte=b)
    #     if any((checkout_overlap, checkin_overlap, both_overlap)):
    #         return False
    #     else:
    #         return True
    #
    # def clean(self):
    #     cid = self.cleaned_data['check_in_date']
    #     cod = self.cleaned_data['check_out_date']
    #     payment = self.cleaned_data['payment']
    #     if cid < datetime.date.today():
    #         raise forms.ValidationError({ "check_in_date":"Date is in the past"})
    #     if cid == cod:
    #         raise forms.ValidationError({ "check_out_date":"Check in date and check out date is the same"})
    #     if not self.dates_is_avaliable(cid, cod):
    #         raise forms.ValidationError({ "check_in_date":"Date is not avaliable",
    #                                       "check_out_date":"Date is not avaliable"})

class ReservationStatusForm(forms.ModelForm):
    class Meta:
        model = Reservation
        fields = ['payment','status',]

class CustomerReservationForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.property_obj = kwargs.pop('property_obj')
        super(CustomerReservationForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Reservation
        fields = [
            # Required for customer
            'customer_name',
            'customer_email',
            'customer_phone_number',
            'check_in_date',
            'check_out_date',
            'payment',
            # Optional for customer
            'customer_notes',
        ]
        help_texts = {
            'check_in_date':'YYYY-MM-DD format',
            'check_out_date':'YYYY-MM-DD format',
            'customer_notes': '(Optional) Send a message for your host'
        }

    # Should have just used one postgresql (if you're on it) query: select 1 from reservations r where (r.start, r.end) overlaps ($form_start, $form_end) limit 1 Uses indices, too.
    def dates_is_avaliable(self,a, b):
        r1,r2,r3 = (Reservation.objects.filter(property_obj=self.property_obj),)*3
        checkout_overlap = r1.filter(check_out_date__gt=a, check_out_date__lte=b)
        checkin_overlap = r2.filter(check_in_date__gte=a, check_in_date__lt=b)
        both_overlap = r3.filter(check_in_date__lte=a, check_out_date__gte=b)
        if any((checkout_overlap, checkin_overlap, both_overlap)):
            return False
        else:
            return True

    def verify_price(self,a,b,payment):
        normal_price = self.property_obj.normal_price
        special_price = self.property_obj.special_price if self.property_obj.special_price else normal_price
        total_days = b - a
        full_price = 0
        for x in range(total_days.days):
            if a.isoweekday()==5 or a.isoweekday()==6: # check in friday and saturday
                full_price += special_price
                a = a + datetime.timedelta(days=1)
            else:
                full_price += normal_price
                a = a + datetime.timedelta(days=1)
        return (full_price,full_price/2)

    def clean(self):
        cid = self.cleaned_data['check_in_date']
        cod = self.cleaned_data['check_out_date']
        payment = self.cleaned_data['payment']
        if cid < datetime.date.today():
            raise forms.ValidationError({ "check_in_date":"Date is in the past"})
        if cid == cod:
            raise forms.ValidationError({ "check_out_date":"Check in date and check out date is the same"})
        if not self.dates_is_avaliable(cid, cod):
            raise forms.ValidationError({ "check_in_date":"Date is not avaliable",
                                          "check_out_date":"Date is not avaliable"})
        if payment not in self.verify_price(cid,cod,payment):
            raise forms.ValidationError({"check_out_date":"Payment is neither full or down payment"})
