from django.db import models
from django.db.models.query import QuerySet
from datetime import time
from django.contrib.auth.models import User

class SoftDeletionQuerySet(QuerySet):
    def delete(self):
        return super(SoftDeletionQuerySet, self).update(deleted_at=timezone.now())

    def hard_delete(self):
        return super(SoftDeletionQuerySet, self).delete()

    def alive(self):
        return self.filter(deleted_at=None)
    # Reservation.all_objects.all().dead()
    def dead(self):
        return self.exclude(deleted_at=None)

class SoftDeletionManager(models.Manager):
    def __init__(self, *args, **kwargs):
        self.alive_only = kwargs.pop('alive_only', True)
        super(SoftDeletionManager, self).__init__(*args, **kwargs)

    def get_queryset(self):
        if self.alive_only:
            return SoftDeletionQuerySet(self.model).filter(deleted_at=None)
        return SoftDeletionQuerySet(self.model)

    def hard_delete(self):
        return self.get_queryset().hard_delete()

class SoftDeletionModel(models.Model):
    deleted_at = models.DateTimeField(blank=True, null=True)

    objects = SoftDeletionManager()
    all_objects = SoftDeletionManager(alive_only=False)

    class Meta:
        abstract = True

    def delete(self):
        self.deleted_at = timezone.now()
        self.save()

    def hard_delete(self):
        super(SoftDeletionModel, self).delete()

class Property(models.Model):
    """Details on property that is going to be made a homestay"""
    # Hidden from form
    date_added = models.DateTimeField(auto_now_add=True)
    archived = models.BooleanField(default=False)
    owner = models.ForeignKey(User)
    slug = models.SlugField(unique=True)
    # Required
    name = models.CharField(max_length=30)
    location = models.CharField(max_length=50)
    property_type = models.CharField(max_length=20)
    max_occupant = models.IntegerField()
    normal_price = models.DecimalField(max_digits=8, decimal_places=2)
    check_in_time = models.TimeField(default=time(14,0))
    check_out_time = models.TimeField(default=time(12,0))
    bank_name = models.CharField(max_length=30)
    account_name = models.CharField(max_length=30)
    account_number = models.CharField(max_length=30)
    # Optional
    special_price = models.DecimalField(blank=True, null=True, max_digits=8, decimal_places=2)
    website = models.URLField(blank=True)
    property_description = models.TextField(blank=True)
    terms_cond = models.TextField(blank=True, verbose_name="Terms and conditions")
    # Premium
    bulk_price = models.DecimalField(blank=True, null=True, max_digits=8, decimal_places=2)
    offer_price = models.DecimalField(blank=True, null=True, max_digits=8, decimal_places=2)
    offer_ongoing = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'properties'

    def __str__(self):
        """Return a string representation of the model."""
        return self.name

STATUS_CHOICES = (
    ('UNPAID', 'Pending payment'),
    ('PAID-DP', 'Paid down payment'),
    ('PAID-FULL', 'Paid full'),
    ('CANCELLED-NR','Cancelled no refund'),
    ('CANCELLED-PR', 'Cancelled paid refund'),
    ('COMPLETED','Completed'),
)


class Reservation(SoftDeletionModel):
    """Reservation for the homestay"""
    # Hidden from form
    date_added = models.DateTimeField(auto_now_add=True)
    archived = models.BooleanField(default=False)
    property_obj = models.ForeignKey(Property, verbose_name='property') # customer shouldn't change this
    bank_reference = models.CharField(max_length=10,blank=True)
    # Required for customer
    customer_name = models.CharField(max_length=30, verbose_name='full name')
    customer_email = models.EmailField(verbose_name='e-mail')
    customer_phone_number = models.CharField(max_length=20, verbose_name='phone number')
    check_in_date = models.DateField()
    check_out_date = models.DateField()
    payment = models.DecimalField(max_digits=8, decimal_places=2)
    # tick box if accept terms and condition
    # accept_booking_cond = models.BooleanField()
    # Optional for customer
    customer_notes = models.TextField(blank=True)
    # For admin
    status = models.CharField(max_length=15,choices=STATUS_CHOICES,default='UNPAID')
    # Optional for admin
    notes = models.TextField(blank=True)

    def __str__(self):
        """Return a string representation of the model."""
        return u'%s %s (%s)' % (self.property_obj.name,\
                                self.check_in_date,self.id)
