# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-16 03:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('booking', '0004_reservation_bank_reference'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reservation',
            name='status',
            field=models.CharField(choices=[('UNPAID', 'Pending payment'), ('PAID-DP', 'Paid D/P'), ('PAID-FULL', 'Paid full'), ('CANCELLED-NR', 'Cancelled no refund'), ('CANCELLED-PR', 'Cancelled paid refund'), ('COMPLETED', 'Completed')], max_length=15),
        ),
    ]
