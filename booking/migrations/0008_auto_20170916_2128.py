# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-16 13:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('booking', '0007_auto_20170916_2112'),
    ]

    operations = [
        migrations.AlterField(
            model_name='property',
            name='slug',
            field=models.CharField(max_length=60, unique=True),
        ),
    ]
