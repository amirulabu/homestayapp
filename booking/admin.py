from django.contrib import admin
from .models import Property, Reservation

class PropertyAdmin(admin.ModelAdmin):
    list_display = ('name', 'location', 'property_type')

class ReservationAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'customer_name', 'status','date_added', 'property_obj','check_in_date', 'check_out_date')
    search_fields = ('name', 'email', 'phone_number')

admin.site.register(Property, PropertyAdmin)
admin.site.register(Reservation, ReservationAdmin)
